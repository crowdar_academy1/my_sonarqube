FROM openjdk:11

WORKDIR /opt

ADD sonarqube.zip /usr/local/bin/sonarqube.zip

RUN groupadd sonarqube && useradd -ms /bin/bash -g sonarqube sonarqube

RUN unzip /usr/local/bin/sonarqube.zip

EXPOSE 9000

COPY /sonarqube-8.9.0.43852/conf/sonar.properties  /usr/local/bin/sonarqube/conf

RUN chown -R sonarqube:sonarqube /opt/sonarqube-8.9.0.43852
USER sonarqube

ENTRYPOINT ["java", "-jar", "/opt/sonarqube-8.9.0.43852/lib/sonar-application-8.9.0.43852.jar", "-Dsonar.log.console=true"]
